package sample.gestioPersisitencia;

import sample.DAO.DAOStudioMongoDB;
import sample.clases.Anime;
import sample.clases.Studio;

import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class GestioPersistenciaMongoDB extends GestioPersistencia {

    //DAOConexioMongoDB conexioMongo = new DAOConexioMongoDB();
    DAOStudioMongoDB studioMongo = new DAOStudioMongoDB();

    @Override
    public void insert(Studio studio) {
        studioMongo.insert(studio);
    }

    @Override
    public void delete(Studio studio) {
        studioMongo.delete(studio);
    }

    @Override
    public void update(Studio studio) {
        studioMongo.update(studio);
    }

    @Override
    public List<Studio> getAll() {
        return studioMongo.getAll();
    }

    @Override
    public Studio getByIdentifier(int id) {
        return studioMongo.getByIdentifier(id);
    }

    @Override
    public void insertAnime(Anime anime) {
        studioMongo.insertAnime(anime);
    }

    @Override
    public void updateAnime(Anime anime) {
        studioMongo.updateAnime(anime);
    }

    @Override
    public void deleteAnime(Studio studio, Anime anime) {
        studioMongo.deleteAnime(studio, anime);
    }

    @Override
    public List<Anime> searchByAnimeName(Studio studio, String name) {
        return studioMongo.searchByAnimeName(studio, name);
    }

    @Override
    public List<Studio> searchByStudioName(String name) {
        return studioMongo.searchByStudioName(name);
    }

    /*@Override
    public List<Anime> searchAnimeNotFinished(Studio studio) {
        return studioMongo.searchAnimeNotFinished(studio);
    }*/
}
