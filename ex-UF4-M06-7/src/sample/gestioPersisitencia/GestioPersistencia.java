package sample.gestioPersisitencia;

import sample.clases.Anime;
import sample.clases.Studio;

import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public abstract class GestioPersistencia {

    public abstract void insert(Studio studio);

    public abstract void delete(Studio studio);

    public abstract void update(Studio studio);

    public abstract List<Studio> getAll();

    public abstract Studio getByIdentifier(int id);

    public abstract void insertAnime(Anime anime);

    public abstract void updateAnime(Anime anime);

    public abstract void deleteAnime(Studio studio,Anime anime);

    public abstract List<Anime> searchByAnimeName(Studio studio, String name);

    public abstract List<Studio> searchByStudioName(String name);

    //public abstract List<Anime> searchAnimeNotFinished(Studio studio);

    //public boolean comprobarIdAnime(int id);
}
