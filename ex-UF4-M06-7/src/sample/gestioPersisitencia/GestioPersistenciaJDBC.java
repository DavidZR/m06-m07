package sample.gestioPersisitencia;

import sample.DAO.DAOConexioJDBC;
import sample.DAO.DAOStudioJDBC;
import sample.clases.Anime;
import sample.clases.Studio;

import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 * 
 */
public class GestioPersistenciaJDBC extends GestioPersistencia {
    DAOConexioJDBC conexioJDBC = new DAOConexioJDBC();
    DAOStudioJDBC studioJDBC = new DAOStudioJDBC();

    @Override
    public void insert(Studio studio) {
        studioJDBC.insert(studio);
    }

    @Override
    public void delete(Studio studio) {
        studioJDBC.delete(studio);
    }

    @Override
    public void update(Studio studio) {
        studioJDBC.update(studio);
    }

    @Override
    public List<Studio> getAll() {
        return studioJDBC.getAll();
    }

    @Override
    public Studio getByIdentifier(int id) {
        return studioJDBC.getByIdentifier(id);
    }

    @Override
    public void insertAnime(Anime anime) {
        studioJDBC.insertAnime(anime);
    }

    @Override
    public void updateAnime(Anime anime) {
        studioJDBC.updateAnime(anime);
    }

    @Override
    public void deleteAnime(Studio studio, Anime anime) {
        studioJDBC.deleteAnime(studio, anime);
    }

    @Override
    public List<Anime> searchByAnimeName(Studio studio, String name) {
        return studioJDBC.searchByAnimeName(studio, name);
    }

    @Override
    public List<Studio> searchByStudioName(String name) {
        return studioJDBC.searchByStudioName(name);
    }

    /*@Override
    public List<Anime> searchAnimeNotFinished(Studio studio) {
        return studioJDBC.searchAnimeNotFinished(studio);
    }*/
}
