package sample.interfaces;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public interface DAOConexio {
    public void open();

    public void close();
}
