package sample.clases;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class Studio {
    int id;
    String name;
    int numTreballadors;
    int animesEnProduccio = 0;
    List<Anime> llistaAnimes = new ArrayList<>();

    public Studio() {
    }

    public Studio(int id, String name, int numTreballadors, List<Anime> llistaAnimes) {
        this.id = id;
        this.name = name;
        this.numTreballadors = numTreballadors;
        this.llistaAnimes = llistaAnimes;
    }

    @Override
    public String toString() {
        return "Studio{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numTreballadors=" + numTreballadors +
                ", animesEnProduccio=" + animesEnProduccio +
                ", llistaAnimes=" + llistaAnimes +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumTreballadors() {
        return numTreballadors;
    }

    public void setNumTreballadors(int numTreballadors) {
        this.numTreballadors = numTreballadors;
    }

    public int getAnimesEnProduccio() {
        return animesEnProduccio;
    }

    public void setAnimesEnProduccio(int animesEnProduccio) {
        this.animesEnProduccio = animesEnProduccio;
    }

    public List<Anime> getLlistaAnimes() {
        return llistaAnimes;
    }

    public void setLlistaAnimes(List<Anime> llistaAnimes) {
        this.llistaAnimes = llistaAnimes;
    }
}
