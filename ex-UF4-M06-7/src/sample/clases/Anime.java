package sample.clases;

import java.util.Date;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class Anime {
    int id;
    String name;
    Date dataInici;
    boolean enProduccio;
    int idStudio;

    public Anime() {
    }

    public Anime(int id, String name, Date dataInici, boolean enProduccio, int idStudio) {
        this.id = id;
        this.name = name;
        this.dataInici = dataInici;
        this.enProduccio = enProduccio;
        this.idStudio = idStudio;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Anime{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dataInici=" + dataInici +
                ", enProduccio=" + enProduccio +
                ", idStudio=" + idStudio +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDataInici() {
        return dataInici;
    }

    public void setDataInici(Date dataInici) {
        this.dataInici = dataInici;
    }

    public boolean isEnProduccio() {
        return enProduccio;
    }

    public void setEnProduccio(boolean enProduccio) {
        this.enProduccio = enProduccio;
    }

    public int getIdStudio() {
        return idStudio;
    }

    public void setIdStudio(int idStudio) {
        this.idStudio = idStudio;
    }
}
