package sample.exceptions;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class ClauException extends Exception {

    public ClauException(String msg) {
        super(msg);
    }
}
