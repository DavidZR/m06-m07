package sample.DAO;

import sample.Log.Log;
import sample.clases.Anime;
import sample.clases.Studio;
import sample.exceptions.ClauException;
import sample.interfaces.DAOStudio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static sample.Controllers.DatabaseController.conexioJDBC;

/**
 * @author David Ortega
 *
 */
public class DAOStudioJDBC implements DAOStudio {
    /**
     * @param studio Objecte que insertara la funcio
     * Inserta un studio en la db
     */
    @Override
    public void insert(Studio studio) {
        // Agafem tots els estudis de la base de dades
        List<Studio> llistaEstudios = getAll();
        boolean estudioRepetit = false;

        // Si el estudi no esta repetit l'insertem
        //if (!estudioRepetit) {
            conexioJDBC.open();

            String sentenciaSQL1 = "INSERT INTO studios (id, name, numTreballadors, animesEnProduccio) values (?,?,?,?)";
            PreparedStatement sentenciaPreparada1 = null;
            try {
                // Comprobem que no existeix cap estudi amb la id del estudi que hem d'insertar
                for (Studio s : llistaEstudios) {
                    if (s.getId() == studio.getId()) {
                        estudioRepetit = true;
                        throw new ClauException("El id no pot ser el mateix");
                        //break;
                    }
                }

                sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
                sentenciaPreparada1.setInt(1, studio.getId());
                sentenciaPreparada1.setString(2, studio.getName());
                sentenciaPreparada1.setInt(3, studio.getNumTreballadors());
                sentenciaPreparada1.setInt(4, studio.getAnimesEnProduccio());
                sentenciaPreparada1.executeUpdate();
                System.out.println("Studio inserit correctament");

                // El codi comentat es per insertar tambe els animes de la llista del estudi,
                // pero no es fa servir.
                /*if (studio.llistaAnimes.size() > 0) {
                    Iterator<Anime> iterAnime = studio.llistaAnimes.iterator();
                    while (iterAnime.hasNext()) {
                        Anime anime = iterAnime.next();

                        java.sql.Date dataSQL = new java.sql.Date(anime.dataInici.getTime());

                        String sentenciaSQL2 = "INSERT INTO animes (id,name,dataInici,enProduccio,id_studio) values (?,?,?,?,?)";
                        PreparedStatement sentenciaPreparada2 = conexioJDBC.con.prepareStatement(sentenciaSQL2);
                        sentenciaPreparada2.setInt(1, anime.id);
                        sentenciaPreparada2.setString(2, anime.name);
                        sentenciaPreparada2.setDate(3, dataSQL);
                        sentenciaPreparada2.setString(3, ((anime.enProduccio) ? "S" : "N"));
                        sentenciaPreparada2.setInt(4, (anime.idStudio));
                        sentenciaPreparada2.executeUpdate();
                        System.out.println("Animes inserits correctament.");
                    }
                } else {
                    System.out.println("El estudi no te animes");
                }*/

            } catch (ClauException e) {
                System.err.println(e);

                String text = "insert DAOStudioJDBC: \n";
                Log.save(text, e);

            } catch (SQLException throwables) {
                //throwables.printStackTrace();
                System.err.println("insert DAOStudioJDBC: " + throwables);

                String text = "insert DAOStudioJDBC: \n";
                Log.save(text, throwables);

            } catch (Exception e) {
                System.err.println("insert DAOStudioJDBC: " + e);

                String text = "insert DAOStudioJDBC: \n";
                Log.save(text, e);

            } finally {
                conexioJDBC.close();
            }
        /*} else {
            System.out.println("L'estudi introduit ja esta en la bd");
        }*/
    }

    /**
     * @param studio Studio que eliminarem
     * Elimina un studio de la db
     */
    @Override
    public void delete(Studio studio) {
        conexioJDBC.open();
        try {
            //Eliminem tots els animes del estudi a eliminar
            String sentenciaSQL2 = "DELETE FROM animes WHERE id_studio = " + studio.getId();
            Statement statement = conexioJDBC.con.createStatement();
            statement.execute(sentenciaSQL2);

            //Eliminem el estudi de la bd
            String sentenciaSQL1 = "DELETE FROM studios WHERE id = " + studio.getId();
            statement = conexioJDBC.con.createStatement();
            statement.execute(sentenciaSQL1);

        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("delete DAOStudioJDBC: " + throwables);
        } catch (Exception e) {
            System.err.println("delete DAOStudioJDBC: " + e);
        } finally {
            conexioJDBC.close();
        }
    }

    /**
     * @param studio Studio que actualitzarem
     * Updateja un studio de la db
     */
    @Override
    public void update(Studio studio) {
        conexioJDBC.open();
        try {
            // Actualitzem les dades del estudi
            String sentenciaSQL1 = "UPDATE studios SET name = ?, animesEnProduccio = ?, numTreballadors = ? WHERE id = ?";
            PreparedStatement sentenciaPreparada1 = null;

            sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
            sentenciaPreparada1.setString(1, studio.getName());
            sentenciaPreparada1.setInt(2, studio.getAnimesEnProduccio());
            sentenciaPreparada1.setInt(3, studio.getNumTreballadors());
            sentenciaPreparada1.setInt(4, studio.getId());
            sentenciaPreparada1.executeUpdate();
            System.out.println("Studio actualitzat correctament.");

            // El codi comentat es per fer un update dels animes de la llista del estudi,
            // pero no es fa servir.
            /*if (studio.llistaAnimes.size() > 0) {
                Iterator<Anime> iterAnime = studio.llistaAnimes.iterator();
                while (iterAnime.hasNext()) {
                    Anime anime = iterAnime.next();

                    java.sql.Date dataSQL = new java.sql.Date(anime.dataInici.getTime());

                    String sentenciaSQL2 = "UPDATE animes SET name = ?, dataInici = ?, enProduccio = ? WHERE id_studio = ?";
                    PreparedStatement sentenciaPreparada2 = conexioJDBC.con.prepareStatement(sentenciaSQL2);
                    sentenciaPreparada2.setString(1, anime.name);
                    sentenciaPreparada2.setDate(2, dataSQL);
                    sentenciaPreparada1.setString(3, ((anime.enProduccio) ? "S" : "N"));
                    sentenciaPreparada2.setInt(4, (anime.idStudio));
                    sentenciaPreparada2.executeUpdate();
                    System.out.println("Animes actualitzats correctament.");
                }
            }*/
        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("update DAOStudioJDBC: " + throwables);

            String text = "update DAOStudioJDBC: \n";
            Log.save(text, throwables);
        } catch (Exception e) {
            System.err.println("update DAOStudioJDBC: " + e);

            String text = "update DAOStudioJDBC: \n";
            Log.save(text, e);
        } finally {
            conexioJDBC.close();
        }
    }

    /**
     * @param anime Anime que insertarem en la db
     * Inserta un animee en la db
     */
    @Override
    public void insertAnime(Anime anime) {
        boolean animeRepetit = false;

        // Agafem tots els animes de la base de dades
        Studio studio = getByIdentifier(anime.getIdStudio());
        List<Anime> llistaAnimes = studio.getLlistaAnimes();


        // Si el anime no esta repetit l'insertem
        //if (!animeRepetit) {
            conexioJDBC.open();

            String sentenciaSQL1 = "INSERT INTO animes (id, name, dataInici, enProduccio, id_studio) values (?,?,?,?,?)";
            PreparedStatement sentenciaPreparada1 = null;

            try {
                // Comprobem que no existeix cap anime amb la id del anime que hem d'insertar
                for (Anime a : llistaAnimes) {
                    if (a.getId() == anime.getId()) {
                        animeRepetit = true;
                        throw new ClauException("L'id no pot ser el mateix");
                        //break;
                    }
                }

                java.sql.Date dataSQL = new java.sql.Date(anime.getDataInici().getTime());

                sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
                sentenciaPreparada1.setInt(1, anime.getId());
                sentenciaPreparada1.setString(2, anime.getName());
                sentenciaPreparada1.setDate(3, dataSQL);
                sentenciaPreparada1.setBoolean(4, anime.isEnProduccio());
                sentenciaPreparada1.setInt(5, (anime.getIdStudio()));
                sentenciaPreparada1.executeUpdate();
                System.out.println("Anime inserit correctament.");

            } catch (ClauException e) {
                System.err.println(e);

                int num = studio.getAnimesEnProduccio() - 1;
                studio.setAnimesEnProduccio(num);
                update(studio);

                String text = "insert DAOStudioJDBC: \n";
                Log.save(text, e);

            } catch (SQLException throwables) {
                //throwables.printStackTrace();
                System.err.println("insertAnime DAOStudioJDBC: " + throwables);

                String text = "insertAnime DAOStudioJDBC: \n";
                Log.save(text, throwables);
            } catch (Exception e) {
                System.err.println("insertAnime DAOStudioJDBC: " + e);

                String text = "insertAnime DAOStudioJDBC: \n";
                Log.save(text, e);
            } finally {
                conexioJDBC.close();
            }
        /*} else {
            int num = studio.getAnimesEnProduccio() - 1;
            studio.setAnimesEnProduccio(num);
            update(studio);
            System.out.println("L'anime introduit ja esta en la bd");
        }*/
    }

    /**
     * @param studio
     * @param anime Anime que eliminarem de la db
     * Elimina un anime de la db
     */
    public void deleteAnime(Studio studio, Anime anime){
        conexioJDBC.open();
        try{
            // Eliminem el anime de la bd
            String sentenciaSQL1 = "DELETE FROM animes WHERE id = " + anime.getId();
            Statement statement = conexioJDBC.con.createStatement();
            statement.execute(sentenciaSQL1);
        }catch (SQLException throwables) {
            System.err.println("deleteAnime DAOStudioJDBC: " + throwables);

            String text = "deleteAnime DAOStudioJDBC: \n";
            Log.save(text, throwables);
        }catch (Exception e) {
            System.err.println("deleteAnime DAOStudioJDBC: " + e);

            String text = "deleteAnime DAOStudioJDBC: \n";
            Log.save(text, e);
        } finally {
            conexioJDBC.close();
        }
    }

    /**
     * @param anime Anime que s'updatejara
     * Updateja un anime de la db
     */
    @Override
    public void updateAnime(Anime anime) {
        conexioJDBC.open();

        //Actualitzem les dades del anime
        String sentenciaSQL1 = "UPDATE animes SET name = ?, dataInici = ?, enProduccio = ? WHERE id_studio = ? and id = ?";
        PreparedStatement sentenciaPreparada1 = null;

        try {
            java.sql.Date dataSQL = new java.sql.Date(anime.getDataInici().getTime());

            sentenciaPreparada1 = conexioJDBC.con.prepareStatement(sentenciaSQL1);
            sentenciaPreparada1.setString(1, anime.getName());
            sentenciaPreparada1.setDate(2, dataSQL);
            sentenciaPreparada1.setBoolean(3, anime.isEnProduccio());
            sentenciaPreparada1.setInt(4, (anime.getIdStudio()));
            sentenciaPreparada1.setInt(5, (anime.getId()));
            sentenciaPreparada1.executeUpdate();
            System.out.println("Anime actualitzat correctament");

        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("updateAnime DAOStudioJDBC: " + throwables);

            String text = "updateAnime DAOStudioJDBC: \n";
            Log.save(text, throwables);

        } catch (Exception e) {
            System.err.println("updateAnime DAOStudioJDBC: " + e);

            String text = "updateAnime DAOStudioJDBC: \n";
            Log.save(text, e);
        } finally {
            conexioJDBC.close();
        }
    }

    /**
     * Recupera tots els studios de la db amb la seva info
     */
    @Override
    public List<Studio> getAll() {
        List<Studio> llistaStudios = new ArrayList<>();
        conexioJDBC.open();

        try {
            String sentenciaSQL = "SELECT id, name, numTreballadors, animesEnProduccio FROM studios";
            Statement statement = null;

            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            // Guardem tots els studis de la bd en una llista
            while (rs.next()) {
                Studio studio = new Studio();
                studio.setId(rs.getInt("id"));
                studio.setName(rs.getString("name"));
                studio.setNumTreballadors(rs.getInt("numTreballadors"));
                studio.setAnimesEnProduccio(rs.getInt("animesEnProduccio"));
                llistaStudios.add(studio);
            }

            //Dades de la taula anime
            String sentenciaSQL2 = "SELECT id,name,dataInici,enProduccio,id_studio FROM animes";
            Statement statement2 = conexioJDBC.con.createStatement();
            statement2 = conexioJDBC.con.createStatement();
            ResultSet rs2 = statement.executeQuery(sentenciaSQL2);

            // Per cada volta del bucle, treballem amb un anime de la bd
            // El bulce acaba quan ja recorregut tota la taula d'animes
            while(rs2.next()) {
                Anime anime = new Anime();
                anime.setId(rs2.getInt("id"));
                anime.setName(rs2.getString("name"));
                anime.setDataInici(rs2.getDate("dataInici"));
                anime.setEnProduccio(rs2.getBoolean("enProduccio"));
                anime.setIdStudio(rs2.getInt("id_studio"));

                // Busquem el estudi al qual perteneix el anime i fiquem el anime en la llista del estudi
                for (Studio studio : llistaStudios) {
                    if (anime.getIdStudio() == studio.getId()) {
                        studio.getLlistaAnimes().add(anime);
                    }
                }
            }
        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("getAll DAOStudioJDBC: " + throwables);
        } catch (NullPointerException e) {
            System.err.println("getAll DAOStudioJDBC: " + e);

            String text = "updateAnime DAOStudioJDBC: \n";
            Log.save(text, e);
        } catch (Exception e) {
            System.err.println("getAll DAOStudioJDBC: " + e);

            String text = "updateAnime DAOStudioJDBC: \n";
            Log.save(text, e);
        } finally {
            conexioJDBC.close();
        }

        return llistaStudios;
    }

    /**
     * @param id Id que farem servir per trobar un studio en la db
     * Recupera un studio de la db mitjanšant un id
     */
    @Override
    public Studio getByIdentifier(Integer id) {
        // Fiquem tots els estudis de la bd en una llista
        List<Studio> llistaStudios = getAll();
        Studio resultat = null;

        // Recorrem la llista i quan un estudi te el mateix id que el que ens han pasat per parametre,
        // el fiquem a la variable "resultat".
        for(Studio studio : llistaStudios){
            if (studio.getId() == id){
                resultat = studio;
            }
        }
        return resultat;
    }

    /**
     * @param studio Studio que farem servir per agafar la llista d'animes
     * @param name Nom que farem servir per buscar animes en la db
     * Recupera una llista d'animes que tenen el nom que se li pasa per parametre
     * 
     */
    @Override
    public List searchByAnimeName(Studio studio, String name) {
        conexioJDBC.open();

        // Fiquem tots els estudis de la bd en una llista
        List<Anime> llistaAnimes = new ArrayList<>();

        // Executem una sentencia que ens treu tots els animes del estudi que ens pasen per parametre,
        // quan el seu nom es igual al que ens pasen per parametre.
        String sentenciaSQL = "SELECT id,name,dataInici,enProduccio,id_studio FROM animes WHERE name = '"+ name +"' AND id_studio = '"+ studio.getId() +"'";
        Statement statement = null;
        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            // Guardem els animes que cumpleixen les condicions en una "llistaAnimes"
            while (rs.next()) {
                Anime anime = new Anime();
                anime.setId(rs.getInt("id"));
                anime.setName(rs.getString("name"));
                anime.setDataInici(rs.getDate("dataInici"));
                anime.setEnProduccio(rs.getBoolean("enProduccio"));
                anime.setIdStudio(rs.getInt("id_studio"));
                llistaAnimes.add(anime);
            }
        }catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("searchByAnimeName DAOStudioJDBC: " + throwables);

            String text = "searchByAnimeName DAOStudioJDBC: \n";
            Log.save(text, throwables);
        }catch (Exception e) {
            System.err.println("searchByAnimeName DAOStudioJDBC: " + e);

            String text = "searchByAnimeName DAOStudioJDBC: \n";
            Log.save(text, e);
        }finally {
            conexioJDBC.close();
        }

        return llistaAnimes;
    }

    /**
     * @param name Nom que farem servir per buscar
     * Recupera un llista de studios que tenen el nom que li pases
     */
    @Override
    public List searchByStudioName(String name) {
        // Fiquem tots els estudis de la bd en una llista
        List<Studio> llistaStudios = getAll();
        List<Studio> resultat = new ArrayList<>();

        // Recorrem la llista i quan un estudi te el mateix nom que el que ens han pasat per parametre,
        // l'afegim a la llista "resultat".
        for(Studio studio : llistaStudios){
            if (studio.getName().equalsIgnoreCase(name)){
                resultat.add(studio);
            }
        }
        return resultat;
    }

    // No s'utilitza
    /*@Override
    public List<Anime> searchAnimeNotFinished(Studio studio) {
        conexioJDBC.open();

        List<Anime> llistaAnimes = new ArrayList<>();

        String sentenciaSQL = "SELECT id,name,dataInici,enProduccio,id_studio FROM animes WHERE enProduccio = 'S'";
        Statement statement = null;
        try {
            statement = conexioJDBC.con.createStatement();
            ResultSet rs = statement.executeQuery(sentenciaSQL);

            while (rs.next()) {
                Anime anime = new Anime();
                anime.id = rs.getInt("id");
                anime.name = rs.getString("name");
                anime.dataInici = rs.getDate("dataInici");
                // .equals comproba que te un valor
                anime.enProduccio =  rs.getBoolean("enProduccio");
                anime.idStudio = rs.getInt("id_studio");
                llistaAnimes.add(anime);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            conexioJDBC.close();
        }

        return llistaAnimes;
    }*/
}