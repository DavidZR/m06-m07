package sample.DAO;

import sample.Log.Log;
import sample.exceptions.ConnexioException;
import sample.interfaces.DAOConexio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author David Ortega
 *
 */
public class DAOConexioJDBC implements DAOConexio {

	
    /**
     * @param con Variable que fem servir per conectarnos a la db
     * @param url Variable que emmagatzema l'url de la db
     * @param usuari Usuari de la db
     * @param password Contra de l'usuari
     */
    Connection con;
    private String url;
    private String usuari;
    private String password;

    public DAOConexioJDBC() {
    }

    /**
     * Obre la connexio amb la db utilitzant les dades de l'usuari
     */
    @Override
    public void open() {
        try {
            // Fas la connexio amb els valors que t'ha pasat l'usuari
            this.con = DriverManager.getConnection(url, usuari, password);
            System.out.println("Connexió oberta correctament");

        } catch (SQLException throwables) {
            //throwables.printStackTrace();
            System.err.println("La connexió no s'ha pogut realiztar correctament");

            String text = "open DAOConexioJDBC: \n";
            Log.save(text, throwables);

        } catch (Exception e) {
            System.err.println("La connexió no s'ha pogut realiztar correctament");

            String text = "open DAOConexioJDBC: \n";
            Log.save(text, e);
        }
    }

    /**
     * Tanca la connexio
     */
    @Override
    public void close() {
        try {
            // Tanquem la connexio
            con.close();
            System.out.println("Connexió tancada correctament");

        } catch (SQLException ex) {
            System.err.println("Error al tancar la connexió");

            String text = "close DAOConexioJDBC: \n";
            Log.save(text, ex);

        } catch (Exception e) {
            System.err.println("Error al tancar la connexió");

            String text = "close DAOConexioJDBC: \n";
            Log.save(text, e);
        }
    }

    // Getters i setters
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsuari() {
        return usuari;
    }

    public void setUsuari(String usuari) {
        this.usuari = usuari;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
