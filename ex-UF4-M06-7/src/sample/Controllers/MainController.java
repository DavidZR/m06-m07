package sample.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.Log.Log;
import sample.clases.Studio;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class MainController {
    private ObservableList<Studio> data = FXCollections.observableArrayList();

    // ID del studi a modificar
    public static int idStudio;

    @FXML
    private TableView<Studio> tableView;

    @FXML
    private Button intentStudio;

    @FXML
    private Button ajustes;

    @FXML
    private Button editar;

    @FXML
    private Button eliminar;

    @FXML
    private TextField searchNomEstudio;

    @FXML
    private Button cercaByNomEstudio;

    @FXML
    private TableColumn<Studio, Integer> columnId;

    @FXML
    private TableColumn<Studio, String> columnName;

    @FXML
    private TableColumn<Studio, Integer> columnNumTreballadors;

    @FXML
    private TableColumn<Studio, Integer> columnAnimesEnProduccio;

    /**
     * Cada vegada que carreguem el main.fxml actualitzem les dades de la taula
     */
    public void initialize() {
        try {
            // Si no hi ha les dades de la conexio no fa res
            if (Main.gestioPersistencia != null) {

                List<Studio> llistaStudios = Main.gestioPersistencia.getAll();

                // Afegeix a la llista tots els estudis
                data.addAll(llistaStudios);

                // S'assignen a les columnes de la taula el valor de l'atribut de l'objecte studio
                columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
                columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
                columnNumTreballadors.setCellValueFactory(new PropertyValueFactory<>("numTreballadors"));
                columnAnimesEnProduccio.setCellValueFactory(new PropertyValueFactory<>("animesEnProduccio"));

                // Fiquem a la taula els valors
                tableView.setItems(data);
            }

        } catch (Exception e) {
            System.err.println("initialize MainController: " + e);

            String text = "initialize MainController: \n";
            Log.save(text, e);
        }
    }

    /**
     * Actualitza la taula
     */
    public void refresh() {
        try {
            // Agafem tots els estudis
            List<Studio> llistaStudios = Main.gestioPersistencia.getAll();

            // Eliminem els valors de la taula
            data.clear();

            // Tornem a fer tot el proces de ficar els elements en la taula
            data.addAll(llistaStudios);

            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
            columnNumTreballadors.setCellValueFactory(new PropertyValueFactory<>("numTreballadors"));
            columnAnimesEnProduccio.setCellValueFactory(new PropertyValueFactory<>("animesEnProduccio"));

            tableView.setItems(data);

        } catch (Exception e) {
            System.err.println("refresh MainController: " + e);

            String text = "refresh MainController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Busca en la taula amb un nom
     */
    @FXML
    public void clickCercaByNomStudio(ActionEvent actionEvent) throws IOException {
        try {
            String nom = searchNomEstudio.getText();

            // Agafem un anime buscant pel nom
            List<Studio> studioByName = Main.gestioPersistencia.searchByStudioName(nom);

            // Refresquem la taula
            data.clear();
            data.addAll(studioByName);

            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
            columnNumTreballadors.setCellValueFactory(new PropertyValueFactory<>("numTreballadors"));
            columnAnimesEnProduccio.setCellValueFactory(new PropertyValueFactory<>("animesEnProduccio"));

            tableView.setItems(data);

        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("clickCercaByNomStudio MainController: " + e);

            String text = "clickCercaByNomStudio MainController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Quan fas click va a un altre fxml per insertar un studio
     */
    @FXML
    public void clickIntentStudio(ActionEvent actionEvent) throws IOException {
        try {
            // Et porta al fxml per crear un studio
            FXMLLoader loader = new FXMLLoader(getClass().getResource("studio.fxml"));
            Stage stage = (Stage) intentStudio.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Estudi");
            stage.setScene(scene);

        } catch (IOException io) {
            System.err.println("clickIntentStudio MainController: " + io);

            String text = "clickIntentStudio MainController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickIntentStudio MainController: " + e);

            String text = "clickIntentStudio MainController: \n";
            Log.save(text, e);
        }
    }


    /**
     * @param actionEvent
     * @throws IOException
     * Quan fas click va a un altre fxml per decidir la db
     */
    @FXML
    public void clickAjustes(ActionEvent actionEvent) throws IOException {
        try {
            java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);

            // Et porta al fxml per escollir la conexio
            FXMLLoader loader = new FXMLLoader(getClass().getResource("database.fxml"));
            Stage stage = (Stage) ajustes.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Ajustes");
            stage.setScene(scene);

        } catch (IOException io) {
            System.err.println("clickAjustes MainController: " + io);

            String text = "clickAjustes MainController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickAjustes MainController: " + e);

            String text = "clickAjustes MainController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     *  Quan fas click va a un altre fxml per editar un studio que hagis seleccionat en a taula
     */
    @FXML
    public void clickEditar(ActionEvent actionEvent) throws IOException {
        java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);

        try {
            // Comprobem que s'ha seleccionat un studio
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                TableView.TableViewSelectionModel selectionModel = tableView.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();

                TablePosition tablePosition = (TablePosition) selectedCells.get(0);

                // Agafem la id de la fila seleccionada
                tablePosition.getTableView().getSelectionModel().getTableView().getId();

                idStudio = tableView.getSelectionModel().getSelectedItem().getId();

                // Canviem de scene
                FXMLLoader loader = new FXMLLoader(getClass().getResource("updateStudio.fxml"));
                Stage stage = (Stage) editar.getScene().getWindow();
                Scene scene = new Scene(loader.load());
                stage.setTitle("Editar estudi");
                stage.setScene(scene);
            }

        } catch (IOException io) {
            System.err.println("clickEditar MainController: " + io);

            String text = "clickEditar MainController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickEditar MainController: " + e);

            String text = "clickEditar MainController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Elimina un studio que hagis seleccionat en la taula quan fas click
     */
    @FXML
    public void clickEliminar(ActionEvent actionEvent) throws IOException {
        java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);

        try {
            //Comprobamos que se ha selecionado un studio
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                TableView.TableViewSelectionModel selectionModel = tableView.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();

                TablePosition tablePosition = (TablePosition) selectedCells.get(0);

                //Agafem la id de la fila seleccionada
                tablePosition.getTableView().getSelectionModel().getTableView().getId();

                int idStudio = tableView.getSelectionModel().getSelectedItem().getId();

                Studio studio = Main.gestioPersistencia.getByIdentifier(idStudio);

                Main.gestioPersistencia.delete(studio);

                //Fem un refresh
                refresh();
            }

        } catch (Exception e) {
            System.err.println("clickEliminar MainController: " + e);

            String text = "clickEliminar MainController: \n";
            Log.save(text, e);
        }
    }
}
