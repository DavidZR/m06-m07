package sample.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sample.Log.Log;
import sample.clases.Anime;
import sample.clases.Studio;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class UpdateStudioController {
    Studio studio = new Studio();

    private ObservableList<Anime> data = FXCollections.observableArrayList();

    //ID del anime a modificar
    public static int idAnime;

    @FXML
    private TableView<Anime> tableView;

    @FXML
    private TextField searchNomAnime;

    @FXML
    private Button cercaByNomAnime;

    @FXML
    private Button intentAnime;

    @FXML
    private Button editar;

    @FXML
    private Button btDesa;

    @FXML
    private Button btCancela;

    @FXML
    private TextField idStudio;

    @FXML
    private TextField nomStudio;

    @FXML
    private TextField numTreballadors;

    @FXML
    private Text numAnimesEnProduccio;

    @FXML
    private TableColumn<Anime, Integer> columnId;

    @FXML
    private TableColumn<Anime, String> columnName;

    @FXML
    private TableColumn<Anime, Date> columnData;

    @FXML
    private TableColumn<Anime, Boolean> columnEnProduccio;

    /**
     * El que pasara cada vegada que s'entri en el fxml
     */
    public void initialize() {
        try {
            // Es fiquen les dades per defecte que te l'estudi en el fxml quan entres en la pantalla d'editar estudi
            studio = Main.gestioPersistencia.getByIdentifier(MainController.idStudio);
            idStudio.setText(String.valueOf(studio.getId()));
            nomStudio.setText(studio.getName());
            numTreballadors.setText(String.valueOf(studio.getNumTreballadors()));
            numAnimesEnProduccio.setText(String.valueOf(studio.getAnimesEnProduccio()));

            //if (Main.gestioPersistencia != null) {
            // Es fiquen tots els animes en la taula
            List<Anime> llistaAnimes = studio.getLlistaAnimes();

            data.addAll(llistaAnimes);

            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
            columnData.setCellValueFactory(new PropertyValueFactory<>("dataInici"));
            columnEnProduccio.setCellValueFactory(new PropertyValueFactory<>("enProduccio"));

            tableView.setItems(data);
            //}

        } catch (Exception e) {
            System.err.println("initialize UpdateStudioController " + e);

            String text = "initialize UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Busca en la taula amb el nom que li pasis
     */
    @FXML
    public void clickCercaByNomAnime(ActionEvent actionEvent) throws IOException {
        try {
            String nom = searchNomAnime.getText();

            // S'agafa l'anime que te el nom que li pases
            List<Anime> animesByName = Main.gestioPersistencia.searchByAnimeName(studio, nom);

            // S'actualitzen les dades de la taula
            data.clear();
            data.addAll(animesByName);

            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
            columnData.setCellValueFactory(new PropertyValueFactory<>("dataInici"));
            columnEnProduccio.setCellValueFactory(new PropertyValueFactory<>("enProduccio"));

            tableView.setItems(data);

        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("clickCercaByNomAnime UpdateStudioController " + e);

            String text = "clickCercaByNomAnime UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * Actualitza la taula
     */
    public void refresh() {
        try {
            Studio s = Main.gestioPersistencia.getByIdentifier(studio.getId());
            List<Anime> llistaAnimes = s.getLlistaAnimes();

            data.clear();

            data.addAll(llistaAnimes);

            columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
            columnData.setCellValueFactory(new PropertyValueFactory<>("dataInici"));
            columnEnProduccio.setCellValueFactory(new PropertyValueFactory<>("enProduccio"));

            tableView.setItems(data);

        } catch (Exception e) {
            System.err.println("refresh UpdateStudioController " + e);

            String text = "refresh UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * T'envia a un fxml per afegir un anime
     */
    @FXML
    public void clickIntentAnime(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("anime.fxml"));
            Stage stage = (Stage) intentAnime.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Anime");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickIntentAnime UpdateStudioController " + io);

            String text = "clickIntentAnime UpdateStudioController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickIntentAnime UpdateStudioController " + e);

            String text = "clickIntentAnime UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Cnacela l'operacio
     */
    @FXML
    public void clickCancela(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
            Stage stage = (Stage) btCancela.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickCancela UpdateStudioController " + io);

            String text = "clickCancela UpdateStudioController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickCancela UpdateStudioController " + e);

            String text = "clickCancela UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Desa les dades noves del studio i l'updateja
     */
    @FXML
    public void clickDesaStudio(ActionEvent actionEvent) throws IOException {
        try {
            // Es guarden les noves dades de l'estudi
            studio.setId(Integer.parseInt(idStudio.getText()));
            studio.setName(nomStudio.getText());
            studio.setNumTreballadors(Integer.parseInt(numTreballadors.getText()));

            // S'updateja l'estudi
            Main.gestioPersistencia.update(studio);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
            Stage stage = (Stage) btDesa.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickDesaStudio UpdateStudioController " + io);

            String text = "clickDesaStudio UpdateStudioController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickDesaStudio UpdateStudioController " + e);

            String text = "clickDesaStudio UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Elimina l'anime que hagis seleccionat en la taula
     */
    @FXML
    public void clickEliminar(ActionEvent actionEvent) throws IOException {
        try {
            //Comprobem que s'ha seleccionat un anime
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                TableView.TableViewSelectionModel selectionModel = tableView.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();

                TablePosition tablePosition = (TablePosition) selectedCells.get(0);

                //Agafem la id de la fila seleccionada
                tablePosition.getTableView().getSelectionModel().getTableView().getId();

                int idAnime = tableView.getSelectionModel().getSelectedItem().getId();

                // getAnimeByIdentifier
                Anime a = new Anime();
                for (Anime anime : studio.getLlistaAnimes()) {
                    if (anime.getId() == idAnime) {
                        a = anime;
                    }
                }

                // Si l'anime que es vol eliminar esta en produccio i el num d'animes en produccio de l'estudi
                // es mes gran que 0 se li resta un quan s'elimini l'anime
                if (a.isEnProduccio() && studio.getAnimesEnProduccio() > 0) {
                    int num = studio.getAnimesEnProduccio() - 1;
                    studio.setAnimesEnProduccio(num);
                    Main.gestioPersistencia.update(studio);
                    numAnimesEnProduccio.setText(String.valueOf(studio.getAnimesEnProduccio()));
                }

                // Eliminem l'anime
                Main.gestioPersistencia.deleteAnime(studio, a);

                //Fem un refresh
                refresh();
            }

        } catch (Exception e) {
            System.err.println("clickEliminar UpdateStudioController " + e);

            String text = "clickEliminar UpdateStudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * T'envia a un nou fxml per editar l'anime seleccionat
     */
    @FXML
    public void clickEditar(ActionEvent actionEvent) throws IOException {
        java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);

        try {
            //Comprobaem que s'ha seleccionat un studio
            if (tableView.getSelectionModel().getSelectedItem() != null) {
                TableView.TableViewSelectionModel selectionModel = tableView.getSelectionModel();
                ObservableList selectedCells = selectionModel.getSelectedCells();

                TablePosition tablePosition = (TablePosition) selectedCells.get(0);

                //Agafem la id de la fila seleccionada
                tablePosition.getTableView().getSelectionModel().getTableView().getId();

                // Actualitzem l'id del anime que utilitzarem posteriorment
                idAnime = tableView.getSelectionModel().getSelectedItem().getId();

                //Canviem de scene
                FXMLLoader loader = new FXMLLoader(getClass().getResource("updateAnime.fxml"));
                Stage stage = (Stage) editar.getScene().getWindow();
                Scene scene = new Scene(loader.load());
                stage.setTitle("Editar anime");
                stage.setScene(scene);
            }

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickEditar UpdateStudioController " + io);

            String text = "clickEditar UpdateStudioController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickEditar UpdateStudioController " + e);

            String text = "clickEditar UpdateStudioController: \n";
            Log.save(text, e);
        }
    }
}
