package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Log.Log;
import sample.clases.Anime;
import sample.clases.Studio;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class AnimeController {
    //Anime anime = new Anime();
    //Studio studio;

    @FXML
    private Button btDesa;

    @FXML
    private Button btCancela;

    @FXML
    private TextField idStudio;

    @FXML
    private TextField idAnime;

    @FXML
    private TextField nomAnime;

    @FXML
    private DatePicker dataInici;

    @FXML
    private RadioButton enProduccioTrue;

    @FXML
    private RadioButton enProduccioFalse;

    /**
     * El que es fara cada vegada que s'entri en el fxml
     */
    public void initialize() {
        idStudio.setText(String.valueOf(MainController.idStudio));
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Cancela la operacio
     */
    @FXML
    public void clickCancela(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("updateStudio.fxml"));
            Stage stage = (Stage) btCancela.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Estudi");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickCancela AnimeController: " + io);

            String text = "clickCancela AnimeController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickCancela AnimeController: " + e);

            String text = "clickCancela AnimeController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Es guarden les dades d'un anime i s'inserta en la db
     */
    @FXML
    public void clickDesaAnime(ActionEvent actionEvent) throws IOException {
        try {
            Anime anime = new Anime();
            // Se li fica al nou anime les dades que hi ha en el fxml
            anime.setId(Integer.parseInt(idAnime.getText()));
            anime.setName(nomAnime.getText());

            LocalDate localDate = dataInici.getValue();
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date dateInici = Date.from(instant);
            anime.setDataInici(dateInici);

            // Agafo un estudi amb l'id declarat de forma estatica
            Studio studio = Main.gestioPersistencia.getByIdentifier(MainController.idStudio);
            // Si el radio button si esta seleccionat es ficara true en l'atribut enProducco de l'anime
            if (enProduccioTrue.isSelected()) {
                anime.setEnProduccio(true);

                // Se li sumara 1 als animes en produccio de l'estudi
                int num = studio.getAnimesEnProduccio() + 1;
                studio.setAnimesEnProduccio(num);
                System.out.println(studio);
                // S'updateja l'estudi
                Main.gestioPersistencia.update(studio);
            }

            // Si el radio button no esta seleccionat es ficara false en l'atribut enProducco de l'anime
            if (enProduccioFalse.isSelected()) {
                anime.setEnProduccio(false);
            }

            anime.setIdStudio(MainController.idStudio);

            // S'inserta l'anime
            Main.gestioPersistencia.insertAnime(anime);

            // Tornem al fxml anterior
            FXMLLoader loader = new FXMLLoader(getClass().getResource("updateStudio.fxml"));
            Stage stage = (Stage) btDesa.getScene().getWindow();
            Scene scene = new Scene(loader.load());

            stage.setTitle("Estudi");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickDesaAnime AnimeController: " + io);

            String text = "clickDesaAnime AnimeController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickDesaAnime AnimeController: " + e);

            String text = "clickDesaAnime AnimeController: \n";
            Log.save(text, e);
        }
    }
}
