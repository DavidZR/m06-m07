package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Log.Log;
import sample.clases.Studio;

import java.io.IOException;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class StudioController {
    @FXML
    private Button btDesa;

    @FXML
    private Button btCancela;

    @FXML
    private TextField idStudio;

    @FXML
    private TextField nomStudio;

    @FXML
    private TextField numTreballadors;

    /**
     * @param actionEvent
     * @throws IOException
     * Cancela la operacio
     */
    @FXML
    public void clickCancela(ActionEvent actionEvent) throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
            Stage stage = (Stage) btCancela.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickCancela StudioController: " + io);

            String text = "clickCancela StudioController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickCancela StudioController: " + e);

            String text = "clickCancela StudioController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Guarda les dades del studio i l'inserta en la db
     */
    @FXML
    public void clickDesaStudio(ActionEvent actionEvent) throws IOException {
        try {
            Studio studio = new Studio();

            // Se li fiquen valors als atributs d'estudi
            studio.setId(Integer.parseInt(idStudio.getText()));
            studio.setName(nomStudio.getText());
            studio.setNumTreballadors(Integer.parseInt(numTreballadors.getText()));

            // S'inserta
            Main.gestioPersistencia.insert(studio);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
            Stage stage = (Stage) btDesa.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            //io.printStackTrace();
            System.err.println("clickDesaStudio StudioController: " + io);

            String text = "clickDesaStudio StudioController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickDesaStudio StudioController: " + e);

            String text = "clickDesaStudio StudioController: \n";
            Log.save(text, e);
        }
    }
}
