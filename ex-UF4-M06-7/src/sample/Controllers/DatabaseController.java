package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DAO.DAOConexioJDBC;
import sample.DAO.DAOConexioMongoDB;
import sample.Log.Log;
import sample.gestioPersisitencia.GestioPersistenciaJDBC;
import sample.gestioPersisitencia.GestioPersistenciaMongoDB;

import java.io.IOException;

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class DatabaseController {
    // Es declara public static per poder fer-les servir en altres clases
    public static DAOConexioJDBC conexioJDBC;
    public static DAOConexioMongoDB conexioMongoDB;

    @FXML
    private RadioButton radioJDBC;

    @FXML
    private RadioButton radioMongoDB;

    @FXML
    private Button btDesa;

    @FXML
    private Button btCancela;

    @FXML
    private TextField host;

    @FXML
    private TextField nomBD;

    @FXML
    private TextField usuari;

    @FXML
    private TextField contra;

    /**
     * @param actionEvent
     * @throws IOException
     * Guarda les dades de la db i inicialitza una connexio
     */
    @FXML
    public void clickDesa(ActionEvent actionEvent) throws IOException {
        try {
            // Inicialitzem els obj en null per quan canviem la connexio de mongo a jdbc per exemple,
            // la connexio de mongo es quedava activada i donava problemes en alguns llocs
            conexioMongoDB = null;
            conexioJDBC = null;

            // Si el radio button selected es el de jdbc s'iguala l'objecte de conexiojdbc
            if (radioJDBC.isSelected()) {
                Main.gestioPersistencia = new GestioPersistenciaJDBC();

                conexioJDBC = new DAOConexioJDBC();

                conexioJDBC.setPassword(contra.getText());
                conexioJDBC.setUsuari(usuari.getText());

                String url = "jdbc:mysql://" + host.getText() + "/" + nomBD.getText() + "?serverTimezone=UTC";
                conexioJDBC.setUrl(url);
            }

            // Si el radio button selected es el de mongo s'iguala l'objecte de conexiomongo
            if (radioMongoDB.isSelected()) {
                Main.gestioPersistencia = new GestioPersistenciaMongoDB();

                conexioMongoDB = new DAOConexioMongoDB();

                conexioMongoDB.setDb(nomBD.getText());
            }

            //Main.setRoot("main.fxml");
            // Torna a la pantalla principal
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
            Stage stage = (Stage) btDesa.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            System.err.println("clickDesa DatabaseController: " + io);

            String text = "clickDesa DatabaseController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickDesa DatabaseController: " + e);

            String text = "clickDesa DatabaseController: \n";
            Log.save(text, e);
        }
    }

    /**
     * @param actionEvent
     * @throws IOException
     * Cancela la operacio
     */
    @FXML
    public void clickCancela(ActionEvent actionEvent) throws IOException {
        try {
            // Torna a la pantalla principal sense fer res
            FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
            Stage stage = (Stage) btCancela.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            stage.setTitle("Pantalla principal");
            stage.setScene(scene);

        } catch (IOException io) {
            System.err.println("clickCancela DatabaseController: " + io);

            String text = "clickCancela DatabaseController: \n";
            Log.save(text, io);

        } catch (Exception e) {
            System.err.println("clickCancela DatabaseController: " + e);

            String text = "clickCancela DatabaseController: \n";
            Log.save(text, e);
        }
    }
}
