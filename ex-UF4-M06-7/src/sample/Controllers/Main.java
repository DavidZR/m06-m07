package sample.Controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.gestioPersisitencia.GestioPersistencia;

import java.io.IOException;
import java.util.logging.Level;

/*toggl taiga

--module-path
/home/users/inf/wiam2/iam21773506/Documents/openjfx-11.0.2_linux-x64_bin-sdk/javafx-sdk-11.0.2/lib
--add-modules
javafx.controls,javafx.fxml

--module-path
"C:\Users\12fra\Documents\Escola del treball\jars\javafx-sdk-11.0.2\lib"
--add-modules
javafx.controls,javafx.fxml*/

/**
 * @author Francesc Banzo
 * @author David Ortega
 *
 */
public class Main extends Application {
    private static Scene scene;
    public static GestioPersistencia gestioPersistencia;

    /**
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);

        //scene = new Scene(loadFXML("Controllers/main"), 602, 400);

        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        primaryStage.setTitle("Pantalla principal");
        primaryStage.setScene(new Scene(root, 602, 400));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
